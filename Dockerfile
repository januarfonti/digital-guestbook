FROM node:14

ARG NUXT_DATABASE_URL

WORKDIR /app

COPY . .

RUN yarn db migrate deploy

ENV HOST 0.0.0.0
EXPOSE 3000

CMD [ "node", ".output/server/index.mjs" ]
